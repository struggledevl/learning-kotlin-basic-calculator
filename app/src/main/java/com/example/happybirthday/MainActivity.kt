package com.example.happybirthday

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Numbers Listeners
        btnDoubleZero.setOnClickListener {}
        btn0.setOnClickListener {}
        btn1.setOnClickListener {}
        btn2.setOnClickListener {}
        btn3.setOnClickListener {}
        btn4.setOnClickListener {}
        btn5.setOnClickListener {}
        btn6.setOnClickListener {}
        btn7.setOnClickListener {}
        btn8.setOnClickListener {}
        btn9.setOnClickListener {}

        // Operators Listeners
        btnClear.setOnClickListener {}
        btnLeftB.setOnClickListener {}
        btnRightB.setOnClickListener {}
        btnDivider.setOnClickListener {}
        btnMultiply.setOnClickListener {}
        btnAdd.setOnClickListener {}
        btnMinus.setOnClickListener {}
        btnDot.setOnClickListener {}
        btnEqual.setOnClickListener {}

    }

    // now create methods
}
